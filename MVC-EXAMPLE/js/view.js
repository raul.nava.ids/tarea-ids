var MVC = {};
MVC.View = class View{
    constructor(elem){
        this.elem = elem;
    }

    eventHandler(){
        document.body.addEventListener('onLoadData', (event) => {
            this.updateView(event.detail);
        });
    }

    notify(data){
        const onLoadDataEvent = new CustomEvent("onLoadData",{detail: data, bubbles: true});
        this.elem.dispatchEvent(onLoadDataEvent);
    }

    updateView(datos){
        for(let key in datos){
            const node = this.elem.querySelector(`#${key}`);
            if(node != null){
                node.value = datos[key];
            }else{
                console.log(`no se encontro ${key}`);
            }
        }
    }
}